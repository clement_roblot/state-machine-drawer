
typedef enum {

    idle,
    connecting,
    waitingConnection,
    processing,
} MainState;


void myStateMachine(void)
{
    static MainState state = idle;
    int a = 2;

    switch(state)
    {
        case idle:
            doSomething();
            state = connecting;
            break;

        case connecting:
            doSomething();
            state = waitingConnection;
            break;
    }
}


void myStateMachine2(void)
{
    static MainState state = idle;
    int a = 2;

    switch(state)
    {

        case waitingConnection:
            doSomething();
            if(a == 2)
                state = processing;
            else
                state = connecting;
            break;

        case processing:
            doSomething();
            break;
    }
}

