#!/usr/bin/env python3
from pycparser import c_parser, c_ast
from graphviz import Digraph


class ConditionVisitor(c_ast.NodeVisitor):

    representation = ""

    def visit_BinaryOp(self, condi):

        self.visit(condi.left)
        self.representation += " " + condi.op + " "
        self.visit(condi.right)

    def visit_ID(self, node):
        self.representation += node.name

    def visit_Constant(self, node):
        self.representation += node.value


class SwitchVisitor(c_ast.NodeVisitor):

    switchCondition = None
    dots = {}
    currentState = None
    graphCounter = 0
    currentCondition = None

    def render(self):

        for i, graph in enumerate(self.dots):
            self.dots[graph].render("output/stateMachine" + str(i) + ".png", view=True)

    def visit_Switch(self, switch):

        # We got a new switch
        self.switchCondition = switch.cond.name

        if self.switchCondition not in self.dots:
            print("New switch")
            self.dots[self.switchCondition] = Digraph()

        # Keep on visiting the rest of the switch
        for c in switch:
            self.visit(c)

        # Once done with this switch we cancel the switch condition
        self.switchCondition = None

    def visit_Case(self, case):

        if self.switchCondition:
            self.dots[self.switchCondition].node(case.expr.name)
            self.currentState = case.expr.name

        # Keep on visiting the rest of the case
        for c in case:
            self.visit(c)

        # Once done with this state we cancel the current state value
        self.currentState = None

    def visit_If(self, condition):

        v = ConditionVisitor()
        v.visit(condition.cond)

        codeCondition = v.representation

        self.currentCondition = codeCondition
        self.visit(condition.iftrue)

        self.currentCondition = "!("+codeCondition+")"
        self.visit(condition.iffalse)

        self.currentCondition = None

    def visit_Assignment(self, assi):

        if self.currentState:
            if assi.lvalue.name == self.switchCondition:
                self.dots[self.switchCondition].edge(self.currentState, assi.rvalue.name, label=self.currentCondition)


if __name__ == "__main__":
    filename = "test.c"

    f = open(filename, "r")
    sourceFile = f.read()

    parser = c_parser.CParser()
    ast = parser.parse(sourceFile, filename=filename)

    v = SwitchVisitor()
    v.visit(ast)

    v.render()
